@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">All Posts</div>

                    <div class="card-body">

                        {{--<a href="{{ route('userpost',$posts->user->id) }}">My Post</a>--}}

                        @foreach($posts as $post)
                            <a href="{{ route('userpost',$post->user->id) }}">  My Post</a>
                            <hr/>
                            <h4>{{ $post->title }}</h4><br/>
                            <h6>Posted By :<b> {{ $post->user->name }}</b></h6><br/>
                            <span>Post Category :<b> <a href="{{ route('category',$post->category->id) }}">{{ $post->category->name }}</a></b></span><br/><br/>
                            <p>{!! $post->description !!} }</p>
                            <hr/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection