@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Category - {{ $category->name }}</div>

                    <div class="card-body">
                        @foreach($category->posts as $post)
                            <hr/>
                            <h4>{{ $post->title }}</h4><br/>
                            <h6>Posted By :<b> {{ $post->user->name }}</b></h6><br/>
                            <p>{!! $post->description !!} }</p>
                            <hr/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection