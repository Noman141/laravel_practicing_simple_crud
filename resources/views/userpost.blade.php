@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">My Posts - User - {{ $user->name }} </div>

                    <div class="card-body">
                        @foreach($user->posts as $post)
                            <hr/>
                            <h4>{{ $post->title }}</h4><br/>
                            <span>Post Category :<b> <a href="{{ route('category',$post->category->id) }}">{{ $post->category->name }}</a></b></span><br/><br/>
                            <p>{!! $post->description !!} }</p>
                            <hr/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection