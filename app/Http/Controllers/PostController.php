<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('welcome');
    }

    public function store(Request $request){
        $post = new  Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->user_id = Auth::user()->id;

        $post->save();
        $post->tags()->sync($request->tags,false);

        return redirect('/home');
    }

    public function allPost(){
        $post = Post::all();
        return view('/post')->with('posts',$post);
    }

    public function getCategory($id){
        $category = Category::find($id);

        return view('/category',compact('category'));
    }

    public function postByUser(){
        $user = Auth::user();
        return view('/userpost',compact('user'));
    }
}
