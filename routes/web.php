<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post', 'PostController@allPost')->name('post');

Route::post('/post/store', 'PostController@store')->name('posts.store');

Route::get('/category/{id}', 'PostController@getCategory')->name('category');

Route::get('/userpost/{id}', 'PostController@postByUser')->name('userpost');
